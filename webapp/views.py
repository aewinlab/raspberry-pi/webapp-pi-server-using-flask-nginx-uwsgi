from flask import render_template, url_for, request, redirect, jsonify

from . import app



@app.route('/', methods=['GET', 'POST'])
@app.route('/index/', methods=['GET', 'POST'])
def index():
	return {
		'content': "hello world from Flask!",
		'status': "done"
	}
